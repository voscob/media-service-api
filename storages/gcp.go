package storages

import (
	"cloud.google.com/go/storage"
	"context"
	"google.golang.org/api/option"
	"io"
)

const (
	ImageHost  = "https://storage.googleapis.com"
	BucketName = "bucket-for-yalantis"
	JsonPath   = "storages/api-yalantis.json"
)

// Upload gets Reader and file name of image
// and uploads it to Google Cloud.
func Upload(r io.Reader, name string) (downloadUrl string, err error) {
	ctx := context.Background()

	client, err := storage.NewClient(ctx, option.WithCredentialsFile(JsonPath))
	if err != nil {
		return
	}

	bh := client.Bucket(BucketName)

	// Next check if the bucket exists
	if _, err = bh.Attrs(ctx); err != nil {
		return
	}

	obj := bh.Object(name)
	w := obj.NewWriter(ctx)
	if _, err = io.Copy(w, r); err != nil {
		return
	}

	if err = w.Close(); err != nil {
		return
	}

	attrs, err := obj.Attrs(ctx)
	if err != nil {
		return
	}
	downloadUrl = attrs.MediaLink

	return
}
