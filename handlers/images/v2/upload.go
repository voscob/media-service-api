package handler_images_v2

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/nfnt/resize"
	"image/jpeg"
	"io/ioutil"
	"media-service-api/auth"
	"media-service-api/database"
	"media-service-api/storages"
	"media-service-api/tools"
	"net/http"
	"strconv"
	"sync"
	"time"
)

// Upload is a function for handler
// which gets "original" image from post-data,
// upload it to storage and insert it to database,
// if takes new dimensions - resizes it,
// upload resized image to storage, insert data of resized image to database
// and returns bundle of "original" image.
//
// API-version: 1
// Details:
// New dimensions are width and height.
func Upload(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Check that it is authorized user
	userId, errorCode, err := auth.CheckAuth(r)
	if errorCode > 0 {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(errorCode, err))
		return
	}

	// Resize dimensions
	var scale float64
	var width, widthResize, height, heightResize int
	var imageBytesResized []byte
	var imageDataResized tools.ImageStruct

	// Get image's data from POST-form
	imageObj, imageHeader, err := r.FormFile("image")
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(302, nil))
		return
	}
	if imageHeader == nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(307, nil))
		return
	}

	// Check that image's type is jpg
	contentType := imageHeader.Header.Get("Content-Type")
	if contentType != "image/jpeg" {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(303, nil))
		return
	}

	imageBytes, err := ioutil.ReadAll(imageObj)
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(101, err))
		return
	}

	// Generate new Uuid for images
	imageUuid := uuid.New().String()
	imageUuidResized := uuid.New().String()

	// Get scale data
	scale, _ = strconv.ParseFloat(r.FormValue("scale"), 64)
	if scale > 0 {
		// Check on for too large or too small scale value
		if scale > 5 {
			scale = 5
		}
		if scale < 0.05 {
			scale = 0.05
		}

		// Convert byte array to image
		imageObj, err := jpeg.Decode(bytes.NewReader(imageBytes))
		if err != nil {
			_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(102, err))
			return
		}

		// Get image dimensions
		width = imageObj.Bounds().Max.X
		height = imageObj.Bounds().Max.Y

		// Format new dimensions
		widthResize = int(float64(width) * scale)

		// Resize image
		resizedImage := resize.Resize(uint(widthResize), uint(heightResize), imageObj, resize.Lanczos3)

		// Fill in the missing data
		heightResize = resizedImage.Bounds().Max.Y

		// Convert resized image to byte array
		bytesBuffer := new(bytes.Buffer)
		err = jpeg.Encode(bytesBuffer, resizedImage, nil)
		imageBytesResized = bytesBuffer.Bytes()
	}

	// Create WaitGroup for goroutines
	var wgStorage sync.WaitGroup
	wgStorage.Add(2)
	// Upload images to the storage
	var downloadUrlOriginal, downloadUrlResized string
	go func() {
		downloadUrlOriginal, err = storages.Upload(bytes.NewReader(imageBytes), fmt.Sprintf("%s.jpg", imageUuid))
		wgStorage.Done()
	}()
	go func() {
		if imageBytesResized != nil {
			downloadUrlResized, err = storages.Upload(bytes.NewReader(imageBytesResized), fmt.Sprintf("%s.jpg", imageUuidResized))
		}
		wgStorage.Done()
	}()
	wgStorage.Wait()

	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(103, err))
		return
	}

	// Format images data
	createDate := time.Now().Unix()

	var imageData tools.ImageStruct
	imageData.Uuid = imageUuid
	imageData.ImageUrl = fmt.Sprintf("%s/%s/%s.jpg", storages.ImageHost, storages.BucketName, imageUuid)
	imageData.DownloadUrl = downloadUrlOriginal
	imageData.ParentId = 0
	imageData.ContentType = contentType
	imageData.Width = width
	imageData.Height = height
	imageData.ImageLength = len(imageBytes)
	imageData.UserId = userId
	imageData.CreateDate = createDate

	if len(downloadUrlResized) > 0 {
		imageDataResized.Uuid = imageUuidResized
		imageDataResized.ImageUrl = fmt.Sprintf("%s/%s/%s.jpg", storages.ImageHost, storages.BucketName, imageUuidResized)
		imageDataResized.DownloadUrl = downloadUrlResized
		imageDataResized.ContentType = contentType
		imageDataResized.Width = widthResize
		imageDataResized.Height = heightResize
		imageDataResized.ImageLength = len(imageBytesResized)
		imageDataResized.UserId = userId
		imageDataResized.CreateDate = createDate
	}

	//Insert data of images to datatable
	var imageId, imageIdResized int64
	var wgData sync.WaitGroup
	wgData.Add(2)
	chData := make(chan int64)
	go func() {
		imageId, _ = database.ImageAdd(imageData)
		imageData.Id = imageId
		chData <- imageId
		wgData.Done()
	}()
	go func() {
		parentId := <-chData
		if len(downloadUrlResized) > 0 {
			imageDataResized.ParentId = parentId
			imageIdResized, err = database.ImageAdd(imageDataResized)
			imageDataResized.Id = imageIdResized
		}
		wgData.Done()

	}()
	wgData.Wait()
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(104, err))
		return
	}

	// Format response
	imageBundleData := tools.ConvertToBundle(imageData)
	if len(imageDataResized.Uuid) > 0 {
		imageBundleData.ChildImages = []tools.ImageStruct{imageDataResized}
	}

	var responseData tools.ResponseBundleStruct
	responseData.Response = imageBundleData
	_ = json.NewEncoder(w).Encode(responseData)
}
