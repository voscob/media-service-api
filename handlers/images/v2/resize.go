package handler_images_v2

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"github.com/nfnt/resize"
	"image/jpeg"
	"io/ioutil"
	"media-service-api/auth"
	"media-service-api/database"
	"media-service-api/storages"
	"media-service-api/tools"
	"net/http"
	"strconv"
	"time"
)

// Resize is a function for handler
// which gets "original" image from database,
// resizes it, upload resized image to storage,
// insert data of resized image to database
// and returns bundle of "original" image.
//
// API-version: 2
// Details:
// New dimension is scale.
func Resize(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Check that it is authorized user
	userId, errorCode, err := auth.CheckAuth(r)
	if errorCode > 0 {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(errorCode, err))
		return
	}

	// Get imageId parameter from url
	var imageId int64

	params := mux.Vars(r)
	if imageIdStr, ok := params["id"]; ok {
		imageId, err = strconv.ParseInt(imageIdStr, 10, 64)
		if err != nil {
			_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(304, nil))
			return
		}
	}

	// Resize variables
	var scale float64
	var widthResize, heightResize int
	var imageBytesResized []byte
	var imageDataResized tools.ImageStruct

	// Get data of image from datatable
	imageData, err := database.ImageGet(imageId)
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(106, err))
		return
	}
	if imageData.Uuid == "" {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(305, nil))
		return
	}
	if imageData.ParentId != 0 {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(308, nil))
		return
	}

	// Get original image by url
	resp, err := http.Get(imageData.ImageUrl)
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(107, err))
		return
	}

	// Get body of request and convert it to byte array
	imageBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(101, err))
		return
	}

	// Get scale parameter
	scale, _ = strconv.ParseFloat(r.FormValue("scale"), 64)
	if scale == 0 {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(301, nil))
		return
	}

	// Check on for too large or too small scale value
	if scale > 5 {
		scale = 5
	}
	if scale < 0.05 {
		scale = 0.05
	}

	// Convert byte array to image
	imageObj, err := jpeg.Decode(bytes.NewReader(imageBytes))
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(102, err))
		return
	}

	// Get image dimensions
	width := imageObj.Bounds().Max.X

	// Format new dimensions
	widthResize = int(float64(width) * scale)

	// Resize image
	resizedImage := resize.Resize(uint(widthResize), uint(heightResize), imageObj, resize.Lanczos3)

	// Fill in the missing data
	if heightResize == 0 {
		heightResize = resizedImage.Bounds().Max.Y
	}

	// Convert resized image to byte array
	bytesBuffer := new(bytes.Buffer)
	err = jpeg.Encode(bytesBuffer, resizedImage, nil)
	imageBytesResized = bytesBuffer.Bytes()

	var downloadUrlResized string
	imageUuidResized := uuid.New().String()
	createDate := time.Now().Unix()
	var imageIdResized int64

	if imageBytesResized != nil {
		downloadUrlResized, err = storages.Upload(bytes.NewReader(imageBytesResized), fmt.Sprintf("%s.jpg", imageUuidResized))
	}

	if len(downloadUrlResized) > 0 {
		imageDataResized.Uuid = imageUuidResized
		imageDataResized.ImageUrl = fmt.Sprintf("%s/%s/%s.jpg", storages.ImageHost, storages.BucketName, imageUuidResized)
		imageDataResized.DownloadUrl = downloadUrlResized
		imageDataResized.ParentId = imageData.Id
		imageDataResized.ContentType = imageData.ContentType
		imageDataResized.Width = widthResize
		imageDataResized.Height = heightResize
		imageDataResized.ImageLength = len(imageBytesResized)
		imageDataResized.UserId = userId
		imageDataResized.CreateDate = createDate

		// Insert data to database
		imageIdResized, err = database.ImageAdd(imageDataResized)
		imageDataResized.Id = imageIdResized
	}

	bundleData := tools.ConvertToBundle(imageData)
	bundleData.ChildImages = []tools.ImageStruct{imageDataResized}

	var responseData tools.ResponseBundleStruct
	responseData.Response = bundleData
	_ = json.NewEncoder(w).Encode(responseData)
}
