package handler_images_v1

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"media-service-api/auth"
	"media-service-api/database"
	"media-service-api/tools"
	"net/http"
	"sort"
	"strconv"
)

// Get is a function for handler
// which returns all or defined bundles of auth user.
func Get(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	// Check that it is authorized user
	userId, errorCode, err := auth.CheckAuth(r)
	if errorCode > 0 {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(errorCode, err))
		return
	}

	// Get imageId parameter from url
	var imageId int64
	params := mux.Vars(r)
	if imageIdStr, ok := params["id"]; ok {
		imageId, err = strconv.ParseInt(imageIdStr, 10, 64)
		if err != nil {
			_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(304, nil))
			return
		}
	}

	// Get bundles from database
	imagesData, err := database.BundlesGet(userId, imageId)
	if err != nil {
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(105, err))
		return
	}

	if imagesData == nil {
		if imageId != 0 {
			_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(305, nil))
		} else {
			_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(306, nil))
		}
		return
	}

	if len(imagesData) == 1 && imagesData[0].ParentId > 0 {
		// Format image response
		var responseImageData tools.ResponseImageStruct
		responseImageData.Response = imagesData[0]
		_ = json.NewEncoder(w).Encode(responseImageData)
	} else {
		bundlesImagesMap := make(map[int64]tools.BundleStruct)
		for _, imageData := range imagesData {
			if imageData.ParentId == 0 {
				bundlesImagesMap[imageData.Id] = tools.ConvertToBundle(imageData)
			} else {
				parentBundle := bundlesImagesMap[imageData.ParentId]
				parentBundle.ChildImages = append(parentBundle.ChildImages, imageData)
				bundlesImagesMap[imageData.ParentId] = parentBundle
			}
		}

		var keys []int64
		for k, _ := range bundlesImagesMap {
			keys = append(keys, k)
		}
		sort.Slice(keys, func(i, j int) bool { return keys[i] < keys[j] })

		var bundlesArr []tools.BundleStruct
		for _, k := range keys {
			bundlesArr = append(bundlesArr, bundlesImagesMap[k])
		}

		// Format response
		if len(keys) > 1 {
			var responseData tools.ResponseBundleArrStruct
			responseData.Response = bundlesArr
			_ = json.NewEncoder(w).Encode(responseData)
		} else {
			var responseData tools.ResponseBundleStruct
			responseData.Response = bundlesArr[0]
			_ = json.NewEncoder(w).Encode(responseData)
		}
	}
}
