package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	handlerImages1 "media-service-api/handlers/images/v1"
	handlerImages2 "media-service-api/handlers/images/v2"
	"media-service-api/tools"
	"net/http"
)

func main() {
	// Create a new server mux and register the handlers
	var router = mux.NewRouter()

	var api = router.PathPrefix("/api").Subrouter()
	api.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(202, nil))
	})

	// API-version 1
	var apiVersion1 = api.PathPrefix("/v1").Subrouter()
	apiVersion1.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(203, nil))
	})
	apiVersion1.HandleFunc("/images", handlerImages1.Get).Methods(http.MethodGet)
	apiVersion1.HandleFunc("/images/{id}", handlerImages1.Get).Methods(http.MethodGet)
	apiVersion1.HandleFunc("/images", handlerImages1.Upload).Methods(http.MethodPost)
	apiVersion1.HandleFunc("/images/{id}", handlerImages1.Resize).Methods(http.MethodPatch)

	// API-version 2
	var apiVersion2 = api.PathPrefix("/v2").Subrouter()
	apiVersion2.NotFoundHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		_ = json.NewEncoder(w).Encode(tools.ErrorDataGet(203, nil))
	})
	apiVersion2.HandleFunc("/images", handlerImages2.Upload).Methods(http.MethodPost)
	apiVersion2.HandleFunc("/images/{id}", handlerImages2.Resize).Methods(http.MethodPatch)

	http.ListenAndServe(":80", router)
}
