package tools

// ErrorDataGet takes inner error code and error object (optional)
// and returns ErrorStruct with explanation of this error.
func ErrorDataGet(errorCode int, err error) (errorStruct ErrorStruct) {
	errorMsg := ""

	switch errorCode {
	case 100:
		errorMsg = "Unexpected error in database during get data of authorized user.\nError: " + err.Error()
	case 101:
		errorMsg = "Unexpected error during read content from the form.\nError: " + err.Error()
	case 102:
		errorMsg = "Unexpected error during decode byte array to image object.\nError: " + err.Error()
	case 103:
		errorMsg = "Unexpected error during upload image to the storage.\nError: " + err.Error()
	case 104:
		errorMsg = "Unexpected error during write data of image to the database.\nError: " + err.Error()
	case 105:
		errorMsg = "Unexpected error during get data of bundles from database.\nError: " + err.Error()
	case 106:
		errorMsg = "Unexpected error during get data of image from database.\nError: " + err.Error()
	case 107:
		errorMsg = "Unexpected error during get data of image from url.\nError: " + err.Error()

	case 200:
		errorMsg = "Authentication failed."
	case 201:
		errorMsg = "Incorrect login or password."
	case 202:
		errorMsg = "API-version not found."
	case 203:
		errorMsg = "API-method not found."

	case 300:
		errorMsg = "Resize parameters are required for this method (\"width\" and/or \"height\")."
	case 301:
		errorMsg = "Resize parameter is required for this method (\"scale\")."
	case 302:
		errorMsg = "Image parameter not found (\"image\")."
	case 303:
		errorMsg = "Content-Type of file is not supported."
	case 304:
		errorMsg = "Invalid parameter \"id\" of image."
	case 305:
		errorMsg = "There is no bundle or image with this id."
	case 306:
		errorMsg = "There are no bundles."
	case 307:
		errorMsg = "Image does not have a header."
	case 308:
		errorMsg = "Only the \"original\" image can be resized."
	}

	errorStruct.Error.ErrorCode = errorCode
	errorStruct.Error.ErrorMsg = errorMsg
	return
}
