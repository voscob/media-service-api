package tools

type ImageStruct struct {
	Id          int64  `json:"id"`
	Uuid        string `json:"uuid"`
	ImageUrl    string `json:"image_url"`
	DownloadUrl string `json:"download_url"`
	ParentId    int64  `json:"parent_id"`
	ContentType string `json:"content_type"`
	Width       int    `json:"width"`
	Height      int    `json:"height"`
	ImageLength int    `json:"image_length"`
	UserId      int64  `json:"user_id"`
	CreateDate  int64  `json:"create_date"`
}

type BundleStruct struct {
	ImageStruct
	ChildImages []ImageStruct `json:"child_images"`
}

type ResponseImageStruct struct {
	Response ImageStruct `json:"response"`
}

type ResponseBundleStruct struct {
	Response BundleStruct `json:"response"`
}

type ResponseBundleArrStruct struct {
	Response []BundleStruct `json:"response"`
}

type ErrorStruct struct {
	Error struct {
		ErrorCode int    `json:"error_code"`
		ErrorMsg  string `json:"error_msg"`
	} `json:"error"`
}

// ConvertToBundle converts ImageStruct to BundleStruct
// with empty ChildImages field.
func ConvertToBundle(imageData ImageStruct) (bundleData BundleStruct) {
	bundleData.Id = imageData.Id
	bundleData.Uuid = imageData.Uuid
	bundleData.ImageUrl = imageData.ImageUrl
	bundleData.DownloadUrl = imageData.DownloadUrl
	bundleData.ParentId = 0
	bundleData.ContentType = imageData.ContentType
	bundleData.Width = imageData.Width
	bundleData.Height = imageData.Height
	bundleData.ImageLength = imageData.ImageLength
	bundleData.UserId = imageData.UserId
	bundleData.CreateDate = imageData.CreateDate
	bundleData.ChildImages = []ImageStruct{}

	return
}
