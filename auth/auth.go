package auth

import (
	"media-service-api/database"
	"net/http"
)

// CheckAuth checks BasicAuth.
// If user is authorized - this method returns an id of users,
// if user is not - returns an inner error code and an object error.
func CheckAuth(r *http.Request) (userId int64, errorCode int, err error) {
	errorCode = 0

	login, password, ok := r.BasicAuth()
	if !ok {
		errorCode = 200
		return
	}

	userId, err = database.UserGetId(login, password)
	if err != nil {
		errorCode = 100
		return
	}
	if userId == 0 {
		errorCode = 201
		return
	}

	return
}
