package database

import (
	"fmt"
)

// UserGetId takes login and password
// and returns id of this user.
func UserGetId(login string, password string) (userId int64, err error) {
	db, err := NewConnection()
	if err != nil {
		return
	}
	defer db.Close()

	rows, err := db.Query(fmt.Sprintf("SELECT user_id FROM users WHERE login = '%s' AND password = '%s' LIMIT 1;", login, password))
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		rows.Scan(&userId)
	}

	return
}
