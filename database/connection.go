package database

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

// NewConnection opens a new connect
// and returns a sql.DB-object.
func NewConnection() (db *sql.DB, err error) {
	db, err = sql.Open("mysql", "voscob:11235813@tcp(localhost:3306)/api-yalantis")
	if err != nil {
		return
	}

	return
}
