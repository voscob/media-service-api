package database

import (
	"database/sql"
	"media-service-api/tools"
)

// ImageAdd adds data of image to database
// and returns last insert id.
func ImageAdd(imageData tools.ImageStruct) (imageId int64, err error) {
	db, err := NewConnection()
	if err != nil {
		return
	}
	defer db.Close()

	result, err := db.Exec("INSERT INTO images (uuid, image_url, download_url, parent_id, content_type, width, height, image_length, user_id, create_date) VALUES(?, ?, ?, ?, ?, ?, ? ,?, ?, ?);",
		imageData.Uuid, imageData.ImageUrl, imageData.DownloadUrl, imageData.ParentId, imageData.ContentType, imageData.Width, imageData.Height, imageData.ImageLength, imageData.UserId, imageData.CreateDate)
	if err != nil {
		return
	}

	imageId, err = result.LastInsertId()
	if err != nil {
		return
	}

	return
}

// ImageGet takes id of image
// and returns data of the image from database.
func ImageGet(imageId int64) (ImageData tools.ImageStruct, err error) {
	db, err := NewConnection()
	if err != nil {
		return
	}
	defer db.Close()

	results, err := db.Query("SELECT id, uuid, image_url, download_url, parent_id, content_type, width, height, image_length, user_id, create_date FROM images WHERE id = ? AND delete_date = 0;", imageId)
	if err != nil {
		return
	}

	for results.Next() {
		err = results.Scan(&ImageData.Id, &ImageData.Uuid, &ImageData.ImageUrl, &ImageData.DownloadUrl, &ImageData.ParentId, &ImageData.ContentType, &ImageData.Width, &ImageData.Height, &ImageData.ImageLength, &ImageData.UserId, &ImageData.CreateDate)
		if err != nil {
			return
		}
	}

	return
}

// BundlesGet takes id of user and id of image
// and returns all bundles of this user
// (if id of image DO pass - returns bundle of this image).
func BundlesGet(userId, imageId int64) (ImagesData []tools.ImageStruct, err error) {
	db, err := NewConnection()
	if err != nil {
		return
	}
	defer db.Close()

	var results *sql.Rows
	if imageId == 0 {
		results, err = db.Query("SELECT id, uuid, image_url, download_url, parent_id, content_type, width, height, image_length, user_id, create_date FROM images WHERE user_id = ? AND delete_date = 0 ORDER BY id ASC;", userId)
	} else {
		results, err = db.Query("SELECT id, uuid, image_url, download_url, parent_id, content_type, width, height, image_length, user_id, create_date FROM images WHERE user_id = ? AND (id = ? OR parent_id = ?) AND delete_date = 0 ORDER BY id ASC;", userId, imageId, imageId)
	}
	if err != nil {
		return
	}

	for results.Next() {
		var imageData tools.ImageStruct
		err = results.Scan(&imageData.Id, &imageData.Uuid, &imageData.ImageUrl, &imageData.DownloadUrl, &imageData.ParentId, &imageData.ContentType, &imageData.Width, &imageData.Height, &imageData.ImageLength, &imageData.UserId, &imageData.CreateDate)
		if err != nil {
			return
		}
		ImagesData = append(ImagesData, imageData)
	}

	return
}
