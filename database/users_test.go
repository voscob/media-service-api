package database

import "testing"

func TestUserGetId(t *testing.T) {
	userId, err := UserGetId("user1", "1234")
	if err != nil || (err == nil && userId != 1) {
		t.Errorf("UserGetId(\"user1\", \"1234\") = return %d; want 4", userId)
	}
}
