# Media Service API for image processing
**Media Service API for image processing** &mdash; is a service which allows a user to resize and upload photos to the cloud storage.

## URL:
```bash
/api/v{id_of_version_of_API}/{method_name}
```

## API versioning mechanism `{id_of_version_of_API}`:
Service has versioning mechanism which for now has just two versions of some methods. Version of API is required parameter which should be passed in request URL.

## Authorization:
All of methods can only be called with basic access authentication.

## Responses:
All methods return JSON-data has first key is `response` or `error`.<br> 
Response-value can be in three versions: data of image, data of bundle and data of array of bundles.<br>
Error-value - is a data of error.

## Methods `{method_name}`:
- ### Returns all bundles of auth user.
    Request: `/images`<br>
    HTTP method: `GET`<br>
    Required parameters: -<br> 
    Optional parameters: -<br><br>

    Example:<br>
    Request: `/api/v1/images`<br>
    HTTP method: `GET`<br>
    Response:
 ```json
{
	"response": [{
			"id": 68,
			"uuid": "f2dfbe85-8dc1-4cc2-a08a-40a23929dced",
			"image_url": "https://storage.googleapis.com/bucket-for-yalantis/f2dfbe85-8dc1-4cc2-a08a-40a23929dced.jpg",
			"download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/f2dfbe85-8dc1-4cc2-a08a-40a23929dced.jpg?generation=1587895738131167&alt=media",
			"parent_id": 0,
			"content_type": "image/jpeg",
			"width": 700,
			"height": 1050,
			"image_length": 133810,
			"user_id": 1,
			"create_date": 1587895737,
			"child_images": []
		},
		{
			"id": 70,
			"uuid": "5fa0342f-3828-4d3c-86c2-6dde7c7d440b",
			"image_url": "https://storage.googleapis.com/bucket-for-yalantis/5fa0342f-3828-4d3c-86c2-6dde7c7d440b.jpg",
			"download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/5fa0342f-3828-4d3c-86c2-6dde7c7d440b.jpg?generation=1587895773099455&alt=media",
			"parent_id": 0,
			"content_type": "image/jpeg",
			"width": 700,
			"height": 1050,
			"image_length": 133810,
			"user_id": 1,
			"create_date": 1587895772,
			"child_images": [{
				"id": 71,
				"uuid": "0b39cb5f-733e-49aa-a959-84edda07d7c6",
				"image_url": "https://storage.googleapis.com/bucket-for-yalantis/0b39cb5f-733e-49aa-a959-84edda07d7c6.jpg",
				"download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/0b39cb5f-733e-49aa-a959-84edda07d7c6.jpg?generation=1587895773092007&alt=media",
				"parent_id": 70,
				"content_type": "image/jpeg",
				"width": 609,
				"height": 913,
				"image_length": 66064,
				"user_id": 1,
				"create_date": 1587895772
			}]
		},
		{
			"id": 72,
			"uuid": "e6781a81-c7ba-4bed-b315-5645f76723b5",
			"image_url": "https://storage.googleapis.com/bucket-for-yalantis/e6781a81-c7ba-4bed-b315-5645f76723b5.jpg",
			"download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/e6781a81-c7ba-4bed-b315-5645f76723b5.jpg?generation=1587895834918676&alt=media",
			"parent_id": 0,
			"content_type": "image/jpeg",
			"width": 700,
			"height": 1050,
			"image_length": 133810,
			"user_id": 1,
			"create_date": 1587895834,
			"child_images": [{
					"id": 73,
					"uuid": "a3719135-ad9a-442a-8a83-1dc47866588f",
					"image_url": "https://storage.googleapis.com/bucket-for-yalantis/a3719135-ad9a-442a-8a83-1dc47866588f.jpg",
					"download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/a3719135-ad9a-442a-8a83-1dc47866588f.jpg?generation=1587895834894102&alt=media",
					"parent_id": 72,
					"content_type": "image/jpeg",
					"width": 609,
					"height": 914,
					"image_length": 66056,
					"user_id": 1,
					"create_date": 1587895834
				},
				{
					"id": 74,
					"uuid": "9af53cbe-75b6-4836-9943-ffb482d546e5",
					"image_url": "https://storage.googleapis.com/bucket-for-yalantis/9af53cbe-75b6-4836-9943-ffb482d546e5.jpg",
					"download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/9af53cbe-75b6-4836-9943-ffb482d546e5.jpg?generation=1587896292525423&alt=media",
					"parent_id": 72,
					"content_type": "image/jpeg",
					"width": 790,
					"height": 1185,
					"image_length": 99045,
					"user_id": 1,
					"create_date": 1587896291
				}
			]
		}
	]
}
```

- ### Returns bundle or image of auth user.
    Request: `/images/{bundle_id_or_image_id}`<br>
    HTTP method: `GET`<br>
    Required parameters:<br>
    - `{bundle_id_or_image_id}` is a URL-parameter which is id of bundle or id of image.<br>
    
    Optional parameters: -<br><br>

    Example #1 (get data of bundle):<br>
    Request: `/api/v1/images/70`<br>
    HTTP method: `GET`<br>
    Response:
 ```json
{
    "response": {
        "id": 70,
        "uuid": "5fa0342f-3828-4d3c-86c2-6dde7c7d440b",
        "image_url": "https://storage.googleapis.com/bucket-for-yalantis/5fa0342f-3828-4d3c-86c2-6dde7c7d440b.jpg",
        "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/5fa0342f-3828-4d3c-86c2-6dde7c7d440b.jpg?generation=1587895773099455&alt=media",
        "parent_id": 0,
        "content_type": "image/jpeg",
        "width": 700,
        "height": 1050,
        "image_length": 133810,
        "user_id": 1,
        "create_date": 1587895772,
        "child_images": [
            {
                "id": 71,
                "uuid": "0b39cb5f-733e-49aa-a959-84edda07d7c6",
                "image_url": "https://storage.googleapis.com/bucket-for-yalantis/0b39cb5f-733e-49aa-a959-84edda07d7c6.jpg",
                "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/0b39cb5f-733e-49aa-a959-84edda07d7c6.jpg?generation=1587895773092007&alt=media",
                "parent_id": 70,
                "content_type": "image/jpeg",
                "width": 609,
                "height": 913,
                "image_length": 66064,
                "user_id": 1,
                "create_date": 1587895772
            }
        ]
    }
}
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Example #2 (get data of image):<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Request: `/api/v1/images/71`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HTTP method: `GET`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Response:
    
 ```json
    {
        "response": {
            "id": 71,
            "uuid": "0b39cb5f-733e-49aa-a959-84edda07d7c6",
            "image_url": "https://storage.googleapis.com/bucket-for-yalantis/0b39cb5f-733e-49aa-a959-84edda07d7c6.jpg",
            "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/0b39cb5f-733e-49aa-a959-84edda07d7c6.jpg?generation=1587895773092007&alt=media",
            "parent_id": 70,
            "content_type": "image/jpeg",
            "width": 609,
            "height": 913,
            "image_length": 66064,
            "user_id": 1,
            "create_date": 1587895772
        }
    }
```
    

- ### Uploads original and resized (optional) images
    Request: `/image`<br>
HTTP method: `POST`<br>
Required parameters:<br>
    - `image` - is a form-data parameter which have to contains image ("image/jpeg" only).<br>
    
    Optional parameters:<br>
    - for `v1` API-version:<br>
		- `width` - is a width resize image;<br>
		- `height` - is a height resize image.<br>
		
	- for `v2` API-version:<br>
	    - `scale` - is a scope resize parameter.<br>
	> Notes:<br>
	    1. if request has at least one of optional parameter - original image (bundle) will has resized image as a children;<br>
	    2. optional parameters `width` and `height` are positive integer with max value 6000;<br>
	    3. optional parameter `scale` is are positive float value with min value 0.05 and max value 5.

    Example #1 (upload image without optional parameters):<br>
    Request: `/api/v1/images` or `/api/v2/images`<br>
    HTTP method: `POST`<br>
    Required parameters: `image`<br>
    Optional parameters: -<br>
    Response:

 ```json
{
    "response": {
        "id": 75,
        "uuid": "851aa8ee-c474-421f-b298-b915eff0dc95",
        "image_url": "https://storage.googleapis.com/bucket-for-yalantis/851aa8ee-c474-421f-b298-b915eff0dc95.jpg",
        "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/851aa8ee-c474-421f-b298-b915eff0dc95.jpg?generation=1587934669427287&alt=media",
        "parent_id": 0,
        "content_type": "image/jpeg",
        "width": 0,
        "height": 0,
        "image_length": 133810,
        "user_id": 1,
        "create_date": 1587934667,
        "child_images": []
    }
}
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Example #2 (upload image with parameter from version API `v1`):<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Request: `/api/v1/images`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HTTP method: `POST`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Required parameters: `image`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Optional parameters: `width`=300<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Response:

 ```json
{
    "response": {
        "id": 77,
        "uuid": "6bd4cbf8-ca10-4370-a3cf-544b68f04705",
        "image_url": "https://storage.googleapis.com/bucket-for-yalantis/6bd4cbf8-ca10-4370-a3cf-544b68f04705.jpg",
        "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/6bd4cbf8-ca10-4370-a3cf-544b68f04705.jpg?generation=1587934877527278&alt=media",
        "parent_id": 0,
        "content_type": "image/jpeg",
        "width": 700,
        "height": 1050,
        "image_length": 133810,
        "user_id": 1,
        "create_date": 1587934875,
        "child_images": [
            {
                "id": 78,
                "uuid": "11104696-2102-4917-a08c-bd3e5161bf3d",
                "image_url": "https://storage.googleapis.com/bucket-for-yalantis/11104696-2102-4917-a08c-bd3e5161bf3d.jpg",
                "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/11104696-2102-4917-a08c-bd3e5161bf3d.jpg?generation=1587934877381103&alt=media",
                "parent_id": 77,
                "content_type": "image/jpeg",
                "width": 300,
                "height": 450,
                "image_length": 21873,
                "user_id": 1,
                "create_date": 1587934875
            }
        ]
    }
}
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Example #3 (upload image with parameter from version API `v2`):<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Request: `/api/v2/images`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HTTP method: `POST`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Required parameters: `image`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Optional parameters: `scale`=1.5<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Response:

 ```json
{
    "response": {
        "id": 85,
        "uuid": "b768f79a-029b-4e80-bfc5-78b4a380f1a5",
        "image_url": "https://storage.googleapis.com/bucket-for-yalantis/b768f79a-029b-4e80-bfc5-78b4a380f1a5.jpg",
        "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/b768f79a-029b-4e80-bfc5-78b4a380f1a5.jpg?generation=1587935193278466&alt=media",
        "parent_id": 0,
        "content_type": "image/jpeg",
        "width": 700,
        "height": 1050,
        "image_length": 133810,
        "user_id": 1,
        "create_date": 1587935191,
        "child_images": [
            {
                "id": 86,
                "uuid": "42735550-9b3f-4041-9bf5-6c39b3db316a",
                "image_url": "https://storage.googleapis.com/bucket-for-yalantis/42735550-9b3f-4041-9bf5-6c39b3db316a.jpg",
                "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/42735550-9b3f-4041-9bf5-6c39b3db316a.jpg?generation=1587935193244058&alt=media",
                "parent_id": 85,
                "content_type": "image/jpeg",
                "width": 1050,
                "height": 1575,
                "image_length": 147875,
                "user_id": 1,
                "create_date": 1587935191
            }
        ]
    }
}
```

- ### Resize already uploaded bundle.
    Request: `/images/{bundle_of_id}`<br>
    HTTP method: `PATCH`<br>
    Required parameters:<br>
    - `{bundle_of_id}` - is URL-parameter which is id of already uploaded bundle (original image).<br><br>
    for `v1` API-version:<br>
	- `width` - is a width resize image<br>
	- `height` - is a height resize image<br><br>
	for `v2` API-version:<br>
	- `scale` - is a scope resize parameter<br><br>
	
	> Notes:<br>
	    1. optional parameters `width` and `height` are positive integer with max value 6000;<br>
		2. optional parameter `scale` is are positive float value with min value 0.05 and max value 5.

    Example #1 (resize image with parameter from version API `v1`):<br>
    Request: `/api/v1/images/85`<br>
    HTTP method: `PATCH`<br>
    Required parameters: `image`, `height`:450<br>
    Optional parameters: -<br>
    Response:

 ```json
{
    "response": {
        "id": 85,
        "uuid": "b768f79a-029b-4e80-bfc5-78b4a380f1a5",
        "image_url": "https://storage.googleapis.com/bucket-for-yalantis/b768f79a-029b-4e80-bfc5-78b4a380f1a5.jpg",
        "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/b768f79a-029b-4e80-bfc5-78b4a380f1a5.jpg?generation=1587935193278466&alt=media",
        "parent_id": 0,
        "content_type": "image/jpeg",
        "width": 700,
        "height": 1050,
        "image_length": 133810,
        "user_id": 1,
        "create_date": 1587935191,
        "child_images": [
            {
                "id": 87,
                "uuid": "a8bc69c3-b7ed-4cc8-b52a-801990979976",
                "image_url": "https://storage.googleapis.com/bucket-for-yalantis/a8bc69c3-b7ed-4cc8-b52a-801990979976.jpg",
                "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/a8bc69c3-b7ed-4cc8-b52a-801990979976.jpg?generation=1587935955063423&alt=media",
                "parent_id": 85,
                "content_type": "image/jpeg",
                "width": 0,
                "height": 450,
                "image_length": 21873,
                "user_id": 1,
                "create_date": 1587935952
            }
        ]
    }
}
```

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Example #2 (resize image with parameter from version API `v2`):<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Request: `/api/v2/images/85`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;HTTP method: `PATCH`<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Required parameters: `image`, `scale`=0.75<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Optional parameters: -<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Response:

 ```json
{
    "response": {
        "id": 85,
        "uuid": "b768f79a-029b-4e80-bfc5-78b4a380f1a5",
        "image_url": "https://storage.googleapis.com/bucket-for-yalantis/b768f79a-029b-4e80-bfc5-78b4a380f1a5.jpg",
        "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/b768f79a-029b-4e80-bfc5-78b4a380f1a5.jpg?generation=1587935193278466&alt=media",
        "parent_id": 0,
        "content_type": "image/jpeg",
        "width": 700,
        "height": 1050,
        "image_length": 133810,
        "user_id": 1,
        "create_date": 1587935191,
        "child_images": [
            {
                "id": 89,
                "uuid": "b2072b16-cba9-4bcc-9cda-9e942475cc44",
                "image_url": "https://storage.googleapis.com/bucket-for-yalantis/b2072b16-cba9-4bcc-9cda-9e942475cc44.jpg",
                "download_url": "https://storage.googleapis.com/download/storage/v1/b/bucket-for-yalantis/o/b2072b16-cba9-4bcc-9cda-9e942475cc44.jpg?generation=1587936173131673&alt=media",
                "parent_id": 85,
                "content_type": "image/jpeg",
                "width": 525,
                "height": 788,
                "image_length": 51889,
                "user_id": 1,
                "create_date": 1587936170
            }
        ]
    }
}
```

## List of errors:

| Error code | Error message |
| ------------- | ------------- |
| 100 | Unexpected error in database during get data of authorized user. |
| 101 | Unexpected error during read content from the form. |
| 102 | Unexpected error during decode byte array to image object. |
| 103 | Unexpected error during upload image to the storage. |
| 104 | Unexpected error during write data of image to the database. |
| 105 | Unexpected error during get data of bundles from database. |
| 106 | Unexpected error during get data of image from database. |
| 107 | Unexpected error during get data of image from url. |
| 200 | Authentication failed. |
| 201 | Incorrect login or password. |
| 202 | API-version not found. |
| 203 | API-method not found. |
| 300 | Resize parameters are required for this method ("width" and/or "height"). |
| 301 | Resize parameter is required for this method ("scale"). |
| 302 | Image parameter not found ("image"). |
| 303 | Content-Type of file is not supported. |
| 304 | Invalid parameter "id" of image. |
| 305 | There is no bundle or image with this id. |
| 306 | There are no bundles. |
| 307 | Image does not have a header. |
| 308 | Only the "original" image can be resized. |

Example (resize image with parameter from version API `v1`, BUT `{bundle_id}` is an id of image):<br>
    Request: `/api/v1/images/89`<br>
    HTTP method: `PATCH`<br>
    Required parameters: `image`, `height`:450<br>
    Optional parameters: -<br>
    Response:

 ```json
{
    "error": {
        "error_code": 308,
        "error_msg": "Only the \"original\" image can be resized."
    }
}
```